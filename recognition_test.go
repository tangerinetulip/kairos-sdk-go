package kairos

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestEnroll(t *testing.T) {
	GetAPIKey()
	GetAppID()

	SetAPIKey("7cfc61faaf9e9fcd17835df92ba2c2ca")
	SetAppID("ea416943")

	bytes, err := ioutil.ReadFile("./test_base64.txt")
	errHelp(err)

	base64Image := string(bytes)
	imageURL := "http://i.imgur.com/795cshT.jpg"
	imagePath := "./test_person.jpg"

	processJSON(Process{Image: "", SubjectID: "TestPerson"}, "enroll")

	feedback := GalleryListAll()
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.GalleryIds) > 0 {
		fmt.Println("List all:")
		for _, gallery := range feedback.GalleryIds {
			fmt.Println(gallery)
		}
	} else {
		t.Error("Failed listing.")
	}

	feedback = EnrollFile(imagePath, "TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println("Enrolled through a file.")
	} else {
		t.Error("Failed enrollment.")
	}

	feedback = EnrollURLOr64(imageURL, "TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println("Enrolled through an URL.")
	} else {
		t.Error("Failed enrollment.")
	}

	feedback = RemoveSubject("TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Status) > 0 {
		fmt.Println(feedback.Status)
	} else {
		t.Error("Failed removal of subject.")
	}

	feedback = EnrollURLOr64(base64Image, "TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println("Enrolled through a base 64 encoded image.")
	} else {
		t.Error("Failed enrollment.")
	}

	feedback = VerifyFile(imagePath, "TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println(feedback.Images[0].Transaction.SubjectID)
	} else {
		t.Error("Failed verification.")
	}

	feedback = VerifyURLOr64(imageURL, "TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println(feedback.Images[0].Transaction.SubjectID)
	} else {
		t.Error("Failed verification.")
	}

	feedback = VerifyURLOr64(base64Image, "TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println(feedback.Images[0].Transaction.SubjectID)
	} else {
		t.Error("Failed verification.")
	}

	feedback = RecognizeFile(imagePath, "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println(feedback.Images[0].Transaction.SubjectID)
	} else {
		t.Error("Failed recognition.")
	}

	feedback = RecognizeURLOr64(imageURL, "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println(feedback.Images[0].Transaction.SubjectID)
	} else {
		t.Error("Failed recognition.")
	}

	feedback = RecognizeURLOr64(base64Image, "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Transaction.Status)
		fmt.Println(feedback.Images[0].Transaction.SubjectID)
	} else {
		t.Error("Failed recognition.")
	}

	feedback = DetectURLOr64(imageURL)
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Faces[0].Attributes.Age)
	} else {
		t.Error("Failed detection of features.")
	}

	feedback = DetectURLOr64(base64Image)
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Images) > 0 {
		fmt.Println(feedback.Images[0].Faces[0].Attributes.Age)
	} else {
		t.Error("Failed detection of features.")
	}

	feedback = ViewGallery("TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.SubjectIds) > 0 {
		for _, subject := range feedback.SubjectIds {
			fmt.Println(subject)
		}
	} else {
		t.Error("Failed viewing a gallery.")
	}

	feedback = ViewSubject("TestPerson", "TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Status) > 0 {
		for _, message := range feedback.Messages {
			fmt.Println(message.FaceID)
		}
	} else {
		t.Error("Failed viewing a subject.")
	}

	feedback = RemoveGallery("TestGallery")
	if len(feedback.Errors) > 0 {
		fmt.Println(feedback.Errors[0].Message)
	} else if len(feedback.Status) > 0 {
		fmt.Println(feedback.Status)
	} else {
		t.Error("Failed removal of gallery.")
	}
}
