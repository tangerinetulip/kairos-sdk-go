package kairos

import (
	"errors"
	"fmt"
)

var (
	apiKey = ""
	appID  = ""
)

func errHelp(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

// SetAPIKey sets Kairos API key for use later
func SetAPIKey(key string) {
	apiKey = key
}

// SetAppID sets Kairos app ID for use later
func SetAppID(ID string) {
	appID = ID
}

// GetAPIKey retrieves Kairos API key
func GetAPIKey() (string, error) {
	if apiKey == "" {
		return "", errors.New("no api key set")
	}
	return apiKey, nil
}

// GetAppID retrieves Kairos app ID
func GetAppID() (string, error) {
	if appID == "" {
		return "", errors.New("no app id set")
	}
	return appID, nil
}
