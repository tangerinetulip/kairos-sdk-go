package kairos

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// EnrollURLOr64 enrolls an image to a subject using an URL or a Base64 encoded image
func EnrollURLOr64(urlOr64 string, subjectID string, galleryName string) Feedback {
	enrollJSON := Process{
		Image:       urlOr64,
		SubjectID:   subjectID,
		GalleryName: galleryName,
	}
	feedback := processJSON(enrollJSON, "enroll")
	return feedback
}

// EnrollFile enrolls an image to a subject by converting the image to Base64 first
func EnrollFile(filepath string, subjectID string, galleryName string) Feedback {
	fileBytes, err := ioutil.ReadFile(filepath)
	errHelp(err)

	base64File := base64.StdEncoding.EncodeToString(fileBytes)

	enrollJSON := Process{
		Image:       base64File,
		SubjectID:   subjectID,
		GalleryName: galleryName,
	}
	feedback := processJSON(enrollJSON, "enroll")
	return feedback
}

// VerifyURLOr64 verifies an image to check if it's the subject using an URL or a Base64 encoded image
func VerifyURLOr64(urlOr64 string, subjectID string, galleryName string) Feedback {
	enrollJSON := Process{
		Image:       urlOr64,
		SubjectID:   subjectID,
		GalleryName: galleryName,
	}
	feedback := processJSON(enrollJSON, "verify")
	return feedback
}

// VerifyFile verifies an image to check if it's the subject by converting the image to Base64 first
func VerifyFile(filepath string, subjectID string, galleryName string) Feedback {
	fileBytes, err := ioutil.ReadFile(filepath)
	errHelp(err)

	base64File := base64.StdEncoding.EncodeToString(fileBytes)

	enrollJSON := Process{
		Image:       base64File,
		SubjectID:   subjectID,
		GalleryName: galleryName,
	}
	feedback := processJSON(enrollJSON, "enroll")
	return feedback
}

// RecognizeURLOr64 tries to match the image with a subject you've enrolled using an URL or Base64 encoded image
func RecognizeURLOr64(urlOr64 string, galleryName string) Feedback {
	recognizeJSON := Process{
		Image:       urlOr64,
		GalleryName: galleryName,
	}
	feedback := processJSON(recognizeJSON, "recognize")
	return feedback
}

// RecognizeFile tries to match the image with a subject you've enrolled by converting the image to Base64 first
func RecognizeFile(filepath string, galleryName string) Feedback {
	fileBytes, err := ioutil.ReadFile(filepath)
	errHelp(err)

	base64File := base64.StdEncoding.EncodeToString(fileBytes)

	enrollJSON := Process{
		Image:       base64File,
		GalleryName: galleryName,
	}
	feedback := processJSON(enrollJSON, "recognize")
	return feedback
}

// DetectURLOr64 detect the face in the image and returns all features by using an URL or Base64 encoded image
func DetectURLOr64(urlOr64 string) Feedback {
	detectURLJSON := Process{
		Image: urlOr64,
	}
	feedback := processJSON(detectURLJSON, "detect")
	return feedback
}

// GalleryListAll lists all galleries that you've made
func GalleryListAll() Feedback {
	listAllJSON := Process{}
	feedback := processJSON(listAllJSON, "listAll")
	return feedback
}

// ViewGallery lists all the subjects in the given gallery
func ViewGallery(galleryName string) Feedback {
	viewGalleryJSON := Process{
		GalleryName: galleryName,
	}
	feedback := processJSON(viewGalleryJSON, "view")
	return feedback
}

// RemoveGallery removes the given gallery from Kairos
func RemoveGallery(galleryName string) Feedback {
	removeSubjectJSON := Process{
		GalleryName: galleryName,
	}

	feedback := processJSON(removeSubjectJSON, "removeGallery")
	return feedback
}

// RemoveSubject removes the given subject from the given gallery
func RemoveSubject(subject string, galleryName string) Feedback {
	removeSubjectJSON := Process{
		SubjectID:   subject,
		GalleryName: galleryName,
	}

	feedback := processJSON(removeSubjectJSON, "removeSubject")
	return feedback
}

// ViewSubject displays all IDs and enrollment timestamps for a given subject
func ViewSubject(subject string, galleryName string) Feedback {
	removeSubjectJSON := Process{
		SubjectID:   subject,
		GalleryName: galleryName,
	}

	feedback := processJSON(removeSubjectJSON, "viewSubject")
	return feedback
}

// processJSON gives provided JSON to Kairos for processing and will return an empty struct if HTTP response is not 200
func processJSON(givenJSON Process, process string) Feedback {
	jsonByte, err := json.Marshal(givenJSON)
	errHelp(err)

	var req *http.Request
	switch process {
	case "enroll":
		req, err = http.NewRequest("POST", "https://api.kairos.com/enroll", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "verify":
		req, err = http.NewRequest("POST", "https://api.kairos.com/verify", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "recognize":
		req, err = http.NewRequest("POST", "https://api.kairos.com/recognize", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "detect":
		req, err = http.NewRequest("POST", "https://api.kairos.com/detect", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "listAll":
		req, err = http.NewRequest("POST", "https://api.kairos.com/gallery/list_all", nil)
		errHelp(err)
	case "view":
		req, err = http.NewRequest("POST", "https://api.kairos.com/gallery/view", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "viewSubject":
		req, err = http.NewRequest("POST", "https://api.kairos.com/gallery/view_subject", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "removeGallery":
		req, err = http.NewRequest("POST", "https://api.kairos.com/gallery/remove", bytes.NewBuffer(jsonByte))
		errHelp(err)
	case "removeSubject":
		req, err = http.NewRequest("POST", "https://api.kairos.com/gallery/remove_subject", bytes.NewBuffer(jsonByte))
		errHelp(err)
	}

	req.Header.Set("Content-Type", "application/json")

	appID, err := GetAppID()
	errHelp(err)
	req.Header.Set("app_id", appID)

	APIKey, err := GetAPIKey()
	errHelp(err)
	req.Header.Set("app_key", APIKey)

	resp, err := http.DefaultClient.Do(req)
	errHelp(err)

	var feedback Feedback
	if resp.StatusCode == 200 {
		bodyBytes, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			errHelp(err)
		} else {
			err := json.Unmarshal(bodyBytes, &feedback)
			errHelp(err)
			return feedback
		}
	}
	return feedback
}
