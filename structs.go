package kairos

// Feedback is Kairos API's JSON output struct for reading
type Feedback struct {
	Status           string   `json:"status"`
	GalleryIds       []string `json:"gallery_ids"`
	Message          string   `json:"message"`
	SubjectIds       []string `json:"subject_ids"`
	UploadedImageURL string   `json:"uploaded_image_url"`
	Images           []struct {
		Status string `json:"status"`
		Width  int    `json:"width"`
		Height int    `json:"height"`
		File   string `json:"file"`
		Faces  []struct {
			TopLeftX        int     `json:"topLeftX"`
			TopLeftY        int     `json:"topLeftY"`
			ChinTipX        int     `json:"chinTipX"`
			RightEyeCenterX int     `json:"rightEyeCenterX"`
			Yaw             int     `json:"yaw"`
			ChinTipY        int     `json:"chinTipY"`
			Confidence      float64 `json:"confidence"`
			Height          int     `json:"height"`
			RightEyeCenterY int     `json:"rightEyeCenterY"`
			Width           int     `json:"width"`
			LeftEyeCenterY  int     `json:"leftEyeCenterY"`
			LeftEyeCenterX  int     `json:"leftEyeCenterX"`
			Pitch           int     `json:"pitch"`
			Attributes      struct {
				Asian  float64 `json:"asian"`
				Gender struct {
					Type string `json:"type"`
				} `json:"gender"`
				Age      int     `json:"age"`
				Hispanic float64 `json:"hispanic"`
				Other    float64 `json:"other"`
				Black    float64 `json:"black"`
				White    float64 `json:"white"`
			} `json:"attributes"`
			FaceID  int     `json:"face_id"`
			Quality float64 `json:"quality"`
			Roll    int     `json:"roll"`
		} `json:"faces"`
		Attributes struct {
			Lips   string  `json:"lips"`
			Asian  float64 `json:"asian"`
			Gender struct {
				Type string `json:"type"`
			} `json:"gender"`
			Age      int     `json:"age"`
			Hispanic float64 `json:"hispanic"`
			Other    float64 `json:"other"`
			Black    float64 `json:"black"`
			White    float64 `json:"white"`
			Glasses  string  `json:"glasses"`
		} `json:"attributes"`
		Transaction struct {
			Status      string  `json:"status"`
			TopLeftX    int     `json:"topLeftX"`
			TopLeftY    int     `json:"topLeftY"`
			GalleryName string  `json:"gallery_name"`
			SubjectID   string  `json:"subject_id"`
			Confidence  float64 `json:"confidence"`
			EyeDistance int     `json:"eyeDistance"`
			Height      int     `json:"height"`
			Width       int     `json:"width"`
			FaceID      int     `json:"face_id"`
			Quality     float64 `json:"quality"`
		} `json:"transaction"`
		Candidates []struct {
			SubjectID           string  `json:"subject_id"`
			FaceID              string  `json:"face_id"`
			Confidence          float64 `json:"confidence"`
			EnrollmentTimestamp string  `json:"enrollment_timestamp"`
		} `json:"candidates"`
	} `json:"images"`
	Messages []struct {
		FaceID              string `json:"face_id"`
		EnrollmentTimestamp string `json:"enrollment_timestamp"`
	} `json:"message"`
	Errors []struct {
		Message string `json:"Message"`
		ErrCode int    `json:"ErrCode"`
	} `json:"Errors"`
}

// Process readies commands for use by the Kairos API
type Process struct {
	Image       string `json:"image,omitempty"`
	SubjectID   string `json:"subject_id,omitempty"`
	GalleryName string `json:"gallery_name,omitempty"`
}
