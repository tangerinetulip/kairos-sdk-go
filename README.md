# kairos-sdk-go

[![Go Report Card](https://goreportcard.com/badge/github.com/tangerinetulip/kairos-sdk-go)](https://goreportcard.com/report/github.com/tangerinetulip/kairos-sdk-go)
[![codecov](https://codecov.io/gh/tangerinetulip/kairos-sdk-go/branch/master/graph/badge.svg)](https://codecov.io/gh/tangerinetulip/kairos-sdk-go)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c1d52206bac7415eabf843416d636c4b)](https://www.codacy.com/app/tangerinetulip/kairos-sdk-go?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=tangerinetulip/kairos-sdk-go&amp;utm_campaign=Badge_Grade)

Go wrapper for the Kairos Facial Recognition API
